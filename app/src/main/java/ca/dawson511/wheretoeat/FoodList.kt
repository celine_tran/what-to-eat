package ca.dawson511.wheretoeat

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.dawson511.wheretoeat.databinding.ActivityFoodListBinding
import java.util.*
import kotlin.collections.ArrayList


class FoodList : AppCompatActivity() {

    private lateinit var binding: ActivityFoodListBinding
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFoodListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //get the food array from main class
        val list = intent?.extras?.getSerializable("foodArray") as ArrayList<Food>

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = FoodListAdapter(list)
        recyclerView.adapter = adapter

        //Adds the food specified in the text box to the list, then update textview
        binding.addButton.setOnClickListener {
            //gets user input and capitalizes it
            val userInput = binding.newFoodText.text.toString()
                .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

            //if the user input is not one of the initial foods, the image is the default, otherwise, assign the right image
            var newFood = Food(userInput, R.drawable.default_food)
            when (userInput) {
                "Burgers" -> newFood = Food(userInput, R.drawable.burgers)
                "Pizza" -> newFood = Food(userInput, R.drawable.pizza)
                "Poutine" -> newFood = Food(userInput, R.drawable.poutine)
                "Salad" -> newFood = Food(userInput, R.drawable.salad)
                "Sushi" -> newFood = Food(userInput, R.drawable.sushi)
            }
            //add the new food to the list and notify the adapter
            list.add(newFood)
            binding.newFoodText.text = null
            adapter.notifyItemInserted(list.size - 1)
        }

        //save button, sends back the new array of food
        binding.saveButton.setOnClickListener {
            val intent = Intent()
            intent.putExtra("NewFoodArray", list)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

}