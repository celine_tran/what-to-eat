package ca.dawson511.wheretoeat

import java.io.Serializable
//data class that holds food information
data class Food (val foodText: String, val foodImage: Int): Serializable