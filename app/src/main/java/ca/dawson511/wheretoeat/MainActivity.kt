package ca.dawson511.wheretoeat


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import ca.dawson511.wheretoeat.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var foodArr = arrayListOf<Food>()

    //gets data back from the foodListAdapter
    private val getResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result != null && result.resultCode == Activity.RESULT_OK) {
                foodArr = result.data?.getSerializableExtra("NewFoodArray") as ArrayList<Food>
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        createFoodArray()
        setRandomFood()

        // click listener for re-roll button
        binding.tryAgain.setOnClickListener { setRandomFood() }

        //click listener for more info. button
        binding.searchButton.setOnClickListener { searchFood() }

        //implement intent that calls FoodList class
        binding.editListButton.setOnClickListener { launchFoodList() }

        //click listener for food picture
        binding.foodImage.setOnClickListener { searchFoodLocation() }
    }

    //creates the initial array list of food with the name and image of each food.
    private fun createFoodArray() {
        val foodArrString: Array<String> = resources.getStringArray(R.array.foods)
        val sushi = Food(foodArrString[0], R.drawable.sushi)
        val pizza = Food(foodArrString[1], R.drawable.pizza)
        val burgers = Food(foodArrString[2], R.drawable.burgers)
        val salad = Food(foodArrString[3], R.drawable.salad)
        val poutine = Food(foodArrString[4], R.drawable.poutine)
        val noodles = Food(foodArrString[5], R.drawable.noodles)
        val pancakes = Food(foodArrString[6], R.drawable.pancakes)
        foodArr.add(sushi)
        foodArr.add(pizza)
        foodArr.add(burgers)
        foodArr.add(salad)
        foodArr.add(poutine)
        foodArr.add(noodles)
        foodArr.add(pancakes)
    }

    //uses the shuffle method to shuffle the array of food and assigning the food at position 0 to image and text
    private fun setRandomFood() {
        foodArr.shuffle()
        binding.foodImage.setImageResource(foodArr[0].foodImage)
        binding.foodText.text = foodArr[0].foodText
    }

    //gets the current food and makes a google search on browser for that food using an intent
    private fun searchFood() {
        val currentFood = binding.foodText.text.toString()
        val myUri: Uri = Uri.parse("https://www.google.com/search?q=$currentFood")
        val intent = Intent(Intent.ACTION_VIEW, myUri)
        startActivity(intent)
    }

    //Uses an explicit intent to launch the foodList class and passing the foodArray
    private fun launchFoodList() {
        val intent = Intent(this, FoodList::class.java)
        intent.putExtra("foodArray", foodArr)
        getResult.launch(intent)
    }

    // Searches for restaurants nearby with associated food that was clicked on
    private fun searchFoodLocation() {
        val food = binding.foodText.text
        val intentUri = Uri.parse("geo:0,0?q=${food}")
        val intent = Intent(Intent.ACTION_VIEW, intentUri)
        intent.setPackage("com.google.android.apps.maps")
        startActivity(intent)
    }
}