package ca.dawson511.wheretoeat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class FoodListAdapter(private val foodList: ArrayList<Food>) :
    RecyclerView.Adapter<FoodListAdapter.MyViewHolder>() {

    //Creates new views with R.layout.car_layout as template
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.card_layout, parent, false)
        return MyViewHolder(view)
    }

    //fills the new view with food names and implements click listener
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.foodTitle.text = foodList[position].foodText
        //listener for the food button click
        holder.foodTitle.setOnClickListener {
            if (foodList.size != 1) {
                //get the text from the button
                val itemToRemove = holder.foodTitle.text.toString()
                val item = foodList.filter { it.foodText == itemToRemove }[0]
                foodList.remove(item)
                //notify removal
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, foodList.size)
            } else {
                Toast.makeText(
                    holder.itemView.context,
                    holder.itemView.resources.getString(R.string.remove_limit_msg),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return foodList.size
    }

    //provides references for elements in the card_layout xml
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var foodTitle: Button

        init {
            foodTitle = itemView.findViewById(R.id.food_btn)
        }
    }
}